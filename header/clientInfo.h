#ifndef _CLIENT_INFO_H_
#define _CLIENT_INFO_H_
#include "User.h"
typedef struct 
{
    int serverSocket;
    FILE* logFile;
    User curUser;
    int running;
    int clientIsShuttingDown;
    int serverIsShuttingDown;
    List* uplSessions;
    List* downloadingFiles;
    int nrDlFiles;
    int nrUplSessions;
    int bruteForce;
    int loggedIn;
} ClientInfo;

void initClientInfo(ClientInfo* cl, int serverSocket,FILE* logfile); 
#endif
