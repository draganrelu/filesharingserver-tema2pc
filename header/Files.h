#ifndef _FILES_H_
#define _FILES_H_
#include "constants.h"
#include "mylist.h"

typedef struct
{
	char name[MAX_FILENAME_LEN];
	char owner[24];
	int type; //0 - private, 1 - shared
	int nrPacksLeft;
} TransferFile;

typedef struct
{
	char name[MAX_FILENAME_LEN];
	int size;
	char owner[24];
	int type; //0 - private, 1 - shared
	
} ServerFile; 

TransferFile* createTransferFile(char* name, char* owner, int type, int nrPacksLeft);

int eqTransferFile(const void* tf1, const void* tf2);
int eqServerFile(const void* sf1, const void* sf2);

ServerFile* createServerFile(char* name, char* owner, int size, int type);

TransferFile* findTransferFile(List* tfiles, char* filename, char* owner);

long int getFileSize(char* filename);

int fileExists(char* filename);








#endif
