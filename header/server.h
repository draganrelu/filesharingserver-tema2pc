#ifndef _SERVER_H_
#define _SERVER_H_
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

#include "ServerInfo.h"
#include "Msg.h"
#include "transferSession.h"

void usage(char*file);
void initServer(ServerInfo* sInfo, char* usrCfgName, char* shrCfgName);
void processCommand(char* command, int clientSock, ServerInfo* sInfo);

void handleServerCommand(char* command, ServerInfo* sInfo);
void manageRequest(int client, char* buf, ServerInfo* serverInfo);




#endif
