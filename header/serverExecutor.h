#ifndef _SERVER_EXECUTOR_H_

#define _SERVER_EXECUTOR_H_
#include "ServerInfo.h"
#include "Msg.h"
#include "Files.h"
#include <string.h>
#include "serverFileHandler.h"
#include "serverPackageHandler.h"
#include "serverSender.h"
#include "transferSession.h"

void executeLogin(char* command, int clientSock, ServerInfo* sInfo);
void executeLogout(char* command, int clientSock, ServerInfo* sInfo);
void executeDelete(char* command, int clientSock, ServerInfo* sInfo);
void executeGetUserList(char* command, int clientSock, ServerInfo* sInfo);
void executeGetFileList(char* command, int clientSock, ServerInfo* sInfo);
void initUpload(int client, Msg* msg, ServerInfo* serverInfo);
void executeUpload(char* command, int clientSock, ServerInfo* sInfo);
void initDownload(ServerFile* file, ServerInfo* sInfo, int nrPacks);
void executeDownload(char* command, int clientSock, ServerInfo* sInfo);
void executeQuit(int clientSock, ServerInfo* sInfo);
void executeShare(char* command, int clientSock, ServerInfo* sInfo);
void executeUnShare(char* command, int clientSock, ServerInfo* sInfo);



#endif
