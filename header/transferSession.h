#ifndef _TRANSFER_SESSION_H_
#define _TRANSFER_SESSION_H_
#include "clientInfo.h"
#include "constants.h"
#include "Msg.h"

typedef struct
{
    int fd;
    int offset;
    char filename[MAX_FILENAME_LEN];
    char owner[24];
    int maxPackSize;
    int nrPacksLeft;
} TransferSession;

int eqSession(const void* a, const void* b);
TransferSession* openSession(char* fname, char* owner, List** sessions, int* nrSessions);
void closeSession(TransferSession* ul, List** sessions, int* nrSessions);
int calculateNrPacks(TransferSession* trs);
void sendNrPacks(TransferSession* trs, int socket);
void sendNextPack(TransferSession* trs, int socket, int type);
TransferSession* findSession(List* sessions, char* filename, char* owner);
void receivePackACK(Msg* msg, List* sessions, int socket, int type);


#endif
