#ifndef _SERVER_SENDER_H_
#define _SERVER_SENDER_H_
#include "Msg.h"
#include "Files.h"
#include <string.h>
#include "ServerInfo.h"
#include <fcntl.h>
#include "serverFileHandler.h"
void sendResponse(int result, MsgType type, int clientSock);
void sendFile(int client, ServerFile* file, ServerInfo* sInfo);
void sendQuitWarningToClients(ServerInfo* sInfo);
void sendShutDownMessageToClients(ServerInfo* sInfo);
void sendResult(int result, int clientSock);
#endif
