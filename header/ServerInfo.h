#ifndef _SERVER_INFO_H_
#define _SERVER_INFO_H_
#include "Files.h"
#include "User.h"
#include "mylist.h"
#include <stdio.h>

typedef struct
{
	FILE* fusrd;
	FILE* fshrd;
	int nrUsers;
	User* users;
	char* connectedClients;
	int nrClientsConnected;
	int maxClientId;
	List* uploadingFiles;
	int nrUplFiles;
	List* downloadingFiles;
	List* dlSessions;
	int nrDlSessions;
	int nrDlFiles;
	int running;
	int shuttingDown;
	
} ServerInfo;
#endif
