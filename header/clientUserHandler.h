#ifndef _CLIENT_USER_HANDLER_H_
#define _CLIENT_USER_HANDLER_H

#include "User.h"
#include "ServerInfo.h"
#include <string.h>

int findUser(User* users, int nrUsers, char* userName);
void addClient(int client, ServerInfo* sInfo);
void removeClient(int client, ServerInfo* sInfo);
void printConnectedClients(ServerInfo* sInfo);

#endif
