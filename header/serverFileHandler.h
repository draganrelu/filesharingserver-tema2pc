#ifndef _SERVER_FILE_HANDLER_H_
#define _SERVER_FILE_HANDLER_H_

#include "ServerInfo.h"
#include "User.h"
#include "mylist.h"
#include <sys/stat.h>
#include <string.h>
#include "constants.h"
#include "Files.h"


void createDirectories(ServerInfo* sInfo);
ServerFile* getUserFile(char* filename, User* usr);
TransferFile* getFileInTransfer(char* filename, ServerInfo* sInfo);
void printTransferFiles(ServerInfo* sInfo);
char* getfilepath(char* filename, char* usrName);
void printFilesOfUsers(ServerInfo* sInfo);
void moveTransferFileToUser(ServerInfo* serverInfo, TransferFile* tf, char* usrName, int type);
void readConfigFiles(ServerInfo* sInfo, char* usrCfgName, char* shrCfgName);
void readPrivateFiles(ServerInfo* sInfo);
void readSharedFiles(ServerInfo* sInfo);
void readUserFile(ServerInfo* sInfo);




#endif
