#ifndef _CONSTANTS_H_
#define _CONSTANTS_H_

#define MAX_CLIENTS 10
#define MAX_FILENAME_LEN 100
#define MAX_FILEPATH_LEN 130
#define PAYLOAD_LEN 1400
#define BUFLEN 1500	
#define MAX_LINE_SIZE 1000
#define MAX_COMMAND_LEN 200

#endif
