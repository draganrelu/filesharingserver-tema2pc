#ifndef _MSG_H_
#define _MSG_H_
#include "constants.h"
typedef enum
{
	INIT,
	COMMAND,
	LOGINRESP,
	LOGOUTRESP,
	UPLRESP,
	DLRESP,
	GFLRESP,
	ERROR,
	PACKUL,
	PACKDL,
	SHRRESP,
	USHRRESP,
	DELETERESP,
	NRPACKS, //nr packs
	CALLBACK, //callback 
	INFO, //information to be printed
	SHUTDOWN,
	QUITWARNING,
	PACKACK
	
	
} MsgType;
typedef struct
{
	MsgType type;
	int len;
	char payload[PAYLOAD_LEN];
} Msg;


void sendMsg(Msg* m, int sd);
void recvMsg(Msg* m, int sd);


#endif
