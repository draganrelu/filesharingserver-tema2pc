#ifndef _SERVER_PACKAGE_HANDLER_H_
#define _SERVER_PACKAGE_HANDLER_H_

#include "Msg.h"
#include "ServerInfo.h"
#include <fcntl.h>
#include <string.h>
#include "serverFileHandler.h"

void sendPackACK(int client, char* filename, char* owner);
void handlePackage(int client, Msg* msg, ServerInfo* serverInfo);

#endif
