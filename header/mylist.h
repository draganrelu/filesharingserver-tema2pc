#ifndef _LIST_H_
#define _LIST_H_
#include <stdio.h>
#include <stdlib.h>
typedef struct listCel

{
    struct listCel* next;
    void* info;

} List;

void addToList(List** list, void* info);
int removeFromList(List** list, void* info, int (*eq)(const void* a, const void* b));
List* findInList(List* list, void* info, int (*eq)(const void* a, const void* b));
#endif
