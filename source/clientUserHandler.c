#include "../header/clientUserHandler.h"
int findUser(User* users, int nrUsers, char* userName)
{
	int i;
	for (i = 0; i < nrUsers; i++)
		if (strcmp(users[i].name, userName) == 0)
			return i;
	return -1;
}
void addClient(int client, ServerInfo* sInfo)
{
	if (client >= sInfo->maxClientId)
	{
		printf("realloc clients\n");
		if (client >= 2 * sInfo->maxClientId)
		{
			sInfo->connectedClients = realloc(sInfo->connectedClients, client + 1);
			memset(sInfo->connectedClients + sInfo->maxClientId, 0, client + 1 - sInfo->maxClientId);
			sInfo->maxClientId = client;
		}
		else
		{
			sInfo->connectedClients = realloc(sInfo->connectedClients, 2 * sInfo->maxClientId);
			memset(sInfo->connectedClients + sInfo->maxClientId, 0, sInfo->maxClientId);
			sInfo->maxClientId *= 2;
		}
	}
	sInfo->nrClientsConnected++;
	sInfo->connectedClients[client] = 1;
}
void removeClient(int client, ServerInfo* sInfo)
{
	sInfo->nrClientsConnected--;
	sInfo->connectedClients[client] = 0;
}
void printConnectedClients(ServerInfo* sInfo)
{
	printf("Connected Clients: ");
	int i;
	for (i = 0; i <= sInfo->maxClientId; i++)
		if (sInfo->connectedClients[i] == 1)
			printf("%d, ", i);
	printf("\n");
}

