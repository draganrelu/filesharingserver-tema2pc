#include "../header/helpers.h"
#include "../header/clientInfo.h"
#include "../header/transferSession.h"
#include "../header/assertCommands.h"
#include "../header/mylist.h"
#include "../header/constants.h"
#include "../header/Msg.h"
#include "../header/Files.h"
#include "../header/User.h"


void usage(char*file)
{
	fprintf(stderr,"Usage: %s ip_server port_server\n",file);
	exit(0);
}
void createLogFile(FILE** logFile)
{
	char filename[100];
	int pid = getpid();
	printf("pid: %d\n", pid);
	sprintf(filename, "client-<%d>.log", pid);
	printf("%s\n", filename);
	*logFile = (FILE*)fopen(filename, "wt");
}
void printPrompt(ClientInfo* cInfo)
{
	if (cInfo->loggedIn)
	{
		printf("%s>", cInfo->curUser.name);
		fprintf(cInfo->logFile,"%s>", cInfo->curUser.name);
		fflush(stdout);
	}
}
void processResult(int res, ClientInfo* cInfo)
{
	char errorMessage[200];
	switch (res)
	{
		case -1:
			strcpy(errorMessage, "-1 Clientul nu e autentificat\n");
			break;
		case -2:
			strcpy(errorMessage, "-2 Sesiune deja deschisa\n");
			break;
		case -3:
			strcpy(errorMessage, "-3 User/parola gresita\n");
			break;
		case -4:
			strcpy(errorMessage, "-4 Fisier inexistent\n");
			break;
		case -5:
			strcpy(errorMessage, "-5 Descarcare interzisa\n");
			break;
		case -6:
			strcpy(errorMessage, "-6 Fisier deja partajat\n");
			break;
		case -7:
			strcpy(errorMessage, "-7 Fisier deja privat\n");
			break;
		case -8:
			strcpy(errorMessage, "-8 Brute-force detectat\n");
			break;
		case -9:
			strcpy(errorMessage, "-9 Fisier existent pe server\n");
			break;
		case -10:
			strcpy(errorMessage, "-10 Fisier in transfer\n");
			break;
		case -11:
			strcpy(errorMessage, "-11 Utilizator inexistent\n");
			break;
		case -12:
			strcpy(errorMessage, "-12 File is busy\n");
			break;
		default:
			strcpy(errorMessage, "Unknown error code\n");
			break;
	}
	printf("%s", errorMessage);
	fprintf(cInfo->logFile, "%s", errorMessage);
}
void updateCurUser(char* logincommand, ClientInfo* cInfo)
{

	char cpycommand[MAX_COMMAND_LEN];
	strcpy(cpycommand, logincommand);
	strtok(cpycommand, " \n");
	char* usrName = strtok(NULL, " \n");
	char* usrPass = strtok(NULL, " \n");
	strcpy(cInfo->curUser.name, usrName);
	strcpy(cInfo->curUser.passwd, usrPass);
	cInfo->loggedIn = 1;
}

void sendDelete(char* command, ClientInfo* cInfo)
{
	char* filename = strtok(NULL, "\n");
	if (!assert1param(command))
	{
		printf("Invalid delete command format\n");
		printPrompt(cInfo);
		return;
	}
	Msg msg;

	sprintf(msg.payload, "delete %s %s\n", cInfo->curUser.name, filename);
	msg.len = strlen(msg.payload) +1;
	msg.type = COMMAND;
	sendMsg(&msg, cInfo->serverSocket);
}
void sendLogout(char* command, ClientInfo* cInfo)
{
	Msg msg;
	msg.type = COMMAND;
	if (cInfo->loggedIn == 0)
	{
		processResult(-1, cInfo);
	
		return ;
	}
	else
	{
		sprintf(msg.payload, "logout %s\n", cInfo->curUser.name);
		msg.len = strlen(msg.payload) + 1;
		sendMsg(&msg, cInfo->serverSocket);
		cInfo->loggedIn = 0;
	}
}
void sendLogin(char* command, ClientInfo* cInfo)
{
	if (!assert2params(command))
	{
		printf("Invalid login command format\n");
		printPrompt(cInfo);
		return;
	}
	Msg msg;
	msg.type = COMMAND;

	if (cInfo->loggedIn == 1)
	{
		processResult(-2, cInfo);
		printPrompt(cInfo);
		return;
	}
	strcpy(msg.payload, command);
	msg.len = strlen(msg.payload) + 1;
	sendMsg(&msg, cInfo->serverSocket);
}
void responseLogin(char* command, int result, ClientInfo* cInfo)
{
	//receives result + the string of the command sent
	if (result == 0)
	{
		cInfo->bruteForce = 0;
		updateCurUser(command, cInfo);
		return;
	}
	if (result == -3)
	{
		cInfo->bruteForce++;
		if (cInfo->bruteForce == 3)
		{
			cInfo->bruteForce = 0;
			cInfo->clientIsShuttingDown = 1;
		}
		else
			processResult(-3, cInfo);
	}
	else
		processResult(result, cInfo);
	//log

}
void prepareClientForShuttingDown(ClientInfo* cInfo)
{
	printf("Waiting for transfers to complete...\n");
	fprintf(cInfo->logFile, "Waiting for transfers to complete...\n");
	cInfo->clientIsShuttingDown = 1;
}
void sendQuit(char* command, ClientInfo* cInfo)
{
	Msg msg;
	msg.type = COMMAND;
	strcpy(msg.payload, command);
	msg.len = strlen(command) + 1;
	sendMsg(&msg, cInfo->serverSocket);
	printf("Connection finished\n");
	fprintf(cInfo->logFile, "Connection finished\n");
	cInfo->running = 0;
}
void sendGetUserList(char* command, ClientInfo* cInfo)
{
	if (cInfo->loggedIn == 0)
	{
		processResult(-1, cInfo);
		return;
	}

	Msg msg;
	msg.type = COMMAND;
	strcpy(msg.payload, command);
	msg.len = strlen(msg.payload) + 1;
	sendMsg(&msg, cInfo->serverSocket);
}
void sendGetFileList(char* command, ClientInfo* cInfo)
{
	if (!assert1param(command))
	{
		printf("Invalid getfilelist command format\n");
		printPrompt(cInfo);
		return;
	}
	if (cInfo->loggedIn == 0)
		
	{
		processResult(-1, cInfo);
		return;
	}
	Msg msg;
	msg.type = COMMAND;
	strcpy(msg.payload, command);
	msg.len = strlen(msg.payload) + 1;
	sendMsg(&msg, cInfo->serverSocket);
}
void responseDelete(Msg* msg, ClientInfo* cInfo)
{
	int result;
	memcpy(&result, msg->payload, sizeof(int));
	if (result != 0)
		processResult(result, cInfo);
	else
		printf("200 Fisier sters\n");
}
void responseGetFileList(Msg* msg, ClientInfo* cInfo)
{
	int result;
	memcpy(&result, msg->payload, sizeof(int));
	if (result == 0)
	{
		printf("%s", msg->payload+4);
		fprintf(cInfo->logFile, "%s", msg->payload + 4);
	}
	
	else
		processResult(result, cInfo);
}
void sendFile(char* filename, ClientInfo* cInfo)
{
	Msg msg;
	int maxPackSize = PAYLOAD_LEN - MAX_FILENAME_LEN - 1;
	int fd = open(filename, O_RDONLY);
	if (fd < 0)
		printf("error deschidere fisier %s\n", filename);

	int citit;
	int filesize = getFileSize(filename);
	printf("filesize of %s: %d\n", filename, filesize);
	int nrpacks;
	if (filesize % maxPackSize == 0)
		nrpacks = filesize / maxPackSize;
	else
		nrpacks = filesize / maxPackSize + 1;

	//send nr packs + filename
	memcpy(msg.payload, &nrpacks, sizeof(int));
	strcpy(msg.payload + 4, filename);
	msg.len = 5 + strlen(filename);
	msg.type = NRPACKS;
	sendMsg(&msg, cInfo->serverSocket);

	memset(msg.payload, 0, sizeof(msg.payload));
	//send pack by pack - filename + data
	int nameLen = strlen(filename);
	strcpy(msg.payload, filename);
	char* dataPayload = msg.payload + nameLen + 1;
	msg.type = PACKUL;
	while ((citit = read(fd, dataPayload, maxPackSize)) > 0)
	{
		msg.len = citit;
		//printf("trimit packet al fisierului %s\n", msg.payload);
		sendMsg(&msg, cInfo->serverSocket);
	}
}
void sendUpload(char* command, ClientInfo* cInfo)
{
	char* filename = strtok(NULL, "\n");
	if (!assert1param(command))
	{
		printf("Invalid upload command format\n");
		printPrompt(cInfo);
		return;
	}
	if (cInfo->loggedIn == 0)
	{
		processResult(-1, cInfo);
		return;
	}

	if (!fileExists(filename))
	{
		processResult(-4, cInfo);
		printPrompt(cInfo);
		return;
	}

	//trimit upload + nume utilizator + nume fisier
	Msg msg;
	
	sprintf(msg.payload, "upload %s %s\n", cInfo->curUser.name, filename);
	msg.type = COMMAND;
	msg.len = strlen(msg.payload) + 1;
	sendMsg(&msg, cInfo->serverSocket);
}
void responseUpload(Msg* msg, ClientInfo* cInfo)
{
	//receive result -4 or result 0
	int result;
	memcpy(&result, msg->payload, sizeof(int));
	if (result != 0)
	{
		processResult(result, cInfo);
		return;
	}
	TransferSession* upls = openSession(msg->payload+4, cInfo->curUser.name, &cInfo->uplSessions, &cInfo->nrUplSessions);
	sendNrPacks(upls, cInfo->serverSocket);
}
void sendDownload(char* command, ClientInfo* cInfo)
{
	char* owner = strtok(NULL, " \n");
	char modifiedOwner[24];
	char* filename = strtok(NULL, "\n");
	if (!assert2params(command))
	{
		printf("Invalid download command format\n");
		printPrompt(cInfo);
		return;
	}
	if (cInfo->loggedIn == 0)
	{
		processResult(-1, cInfo);
		return;
	}

	
	if (strcmp(owner, "@") == 0)
		strcpy(modifiedOwner, cInfo->curUser.name);
	else
		strcpy(modifiedOwner, owner);
	//check if file is already in transfer
	char filepath[MAX_FILEPATH_LEN];
	sprintf(filepath, "%s/%s", modifiedOwner, filename);
	if (findTransferFile(cInfo->downloadingFiles, filepath, modifiedOwner))
	{
		processResult(-10, cInfo);
		printPrompt(cInfo);
		return;
	}

	//send download curUsrName owner/filename
	Msg msg;
	sprintf(msg.payload, "download %s %s/%s\n", cInfo->curUser.name, modifiedOwner, filename);
	msg.len = strlen(msg.payload) + 1;
	msg.type = COMMAND;
	sendMsg(&msg, cInfo->serverSocket);
}
void sendShare(char* command, ClientInfo* cInfo )
{
	char* filename = strtok(NULL, "\n");
	if (!assert1param(command))
	{
		printf("Invalid share command format\n");
		printPrompt(cInfo);
		return;
	}
	//send share + usrname + filename

	Msg msg;
	sprintf(msg.payload, "share %s %s\n", cInfo->curUser.name, filename);
	msg.len = strlen(msg.payload) + 1;
	msg.type = COMMAND;
	sendMsg(&msg, cInfo->serverSocket);
}
void sendUnShare(char* command, ClientInfo* cInfo)
{
	char* filename = strtok(NULL, "\n");
	if (!assert1param(command))
	{
		printf("Invalid unshare command format\n");
		printPrompt(cInfo);
		return;
	}
	//send unshare + usrname + filename

	Msg msg;
	sprintf(msg.payload, "unshare %s %s\n", cInfo->curUser.name, filename);
	msg.len = strlen(msg.payload) + 1;
	msg.type = COMMAND;
	sendMsg(&msg, cInfo->serverSocket);
}
void responseShare(Msg* msg, ClientInfo* cInfo)
{
	int result;
	memcpy(&result, msg->payload, sizeof(int));
	if (result != 0)
		processResult(result, cInfo);
}
void responseUnShare(Msg* msg, ClientInfo* cInfo)
{
	int result;
	memcpy(&result, msg->payload, sizeof(int));
	if (result != 0)
		processResult(result, cInfo);
	
}
void responseDownload(Msg* msg, ClientInfo* cInfo)
{
	int result;
	memcpy(&result, msg->payload, sizeof(int));

	if (result != 0)
	{
		processResult(result, cInfo);
		return;
	}
}
void sendPackACK(char* filepath, ClientInfo* cInfo)
{
	//pack ack contains owner + filename
	Msg msg;
	msg.type = PACKACK;
	char* owner = cInfo->curUser.name;
	strcpy(msg.payload, owner);
	strcpy(msg.payload + strlen(owner) + 1, filepath);
	msg.len = strlen(owner) + strlen(filepath) + 2;
	sendMsg(&msg, cInfo->serverSocket);
}

void initDownload(Msg* msg, ClientInfo* cInfo)
{
	//payload : nrpacks(int) + owner + filepath
	//owner is actually the name of current user, not needed
	int nrPacks;

	memcpy(&nrPacks, msg->payload, sizeof(int));
	char* owner = msg->payload + 4;
	char* filepath = msg->payload + 4 + strlen(owner) + 1;
	TransferFile* newtfile = createTransferFile(filepath, owner, 1, nrPacks);
	cInfo->nrDlFiles++;
	addToList(&cInfo->downloadingFiles, newtfile);
	sendPackACK(filepath, cInfo);
}
void handlePackage(Msg* msg, ClientInfo* cInfo)
{
	//pack contains owner + filepath + data
	//owner is actually curUser

	char* owner = msg->payload;
	char* wholeFname = msg->payload + strlen(owner) + 1;
	char* data = msg->payload + strlen(owner) + 1 + strlen(wholeFname) + 1;
	int i = 0;

	//pass username
	while (wholeFname[i] != '/')
		i++;
	i++;
	
	char filename[MAX_FILENAME_LEN];
	strcpy(filename, wholeFname + i);

	int fd = open(filename, O_WRONLY | O_CREAT | O_APPEND, 0644);
	write(fd, data, msg->len);
	close(fd);
	
	TransferFile* dlfile = findTransferFile(cInfo->downloadingFiles, wholeFname, owner);
	dlfile->nrPacksLeft--;
	if (dlfile->nrPacksLeft == 0)
	{
		removeFromList(&cInfo->downloadingFiles, dlfile, eqTransferFile);
		cInfo->nrDlFiles--;
		//sendingCallBACK message
		Msg smsg;
		smsg.type = CALLBACK;
		long int filesz = getFileSize(filename);
		memcpy(smsg.payload, &filesz, sizeof(long int));
		strcpy(smsg.payload + sizeof(long int), owner);
		strcpy(smsg.payload + sizeof(long int) + strlen(owner) + 1, wholeFname);
		smsg.len = strlen(owner) + strlen(wholeFname) + 6;
		printf("Download finished %s - %ld bytes\n", filename, filesz);
		fprintf(cInfo->logFile, "Download finished %s - %ld bytes\n", filename, filesz);
		printPrompt(cInfo);
		sendMsg(&smsg, cInfo->serverSocket);
	}
	else
		sendPackACK(wholeFname, cInfo); 
}
void handleCallback(Msg* msg, ClientInfo* cInfo)
{
	//callback contains filesz + owner + filename
	long int filesz;
	memcpy(&filesz, msg->payload, sizeof(long int));
	char* owner = msg->payload + sizeof(long int);
	char* filename = msg->payload + sizeof(long int) + strlen(owner) + 1;
	printf("Upload finished %s - %ld bytes\n", filename , filesz);
	fprintf(cInfo->logFile, "Upload finished %s - %ld bytes\n", filename, filesz);
	closeSession(findSession(cInfo->uplSessions, filename, owner), &cInfo->uplSessions, &cInfo->nrUplSessions);
}
void sendCommand(char* command, ClientInfo* cInfo)
{
	fprintf(cInfo->logFile, "%s", command);
	if (strcmp(command, "\n") == 0)
	{
		printf("Unknown command\n");
		fprintf(cInfo->logFile, "Unknown command\n");
		printPrompt(cInfo);
		return;
	}


	char cmdcpy[MAX_COMMAND_LEN];

	strcpy(cmdcpy, command);

	char* cmdname = strtok(cmdcpy, " \n");


	if (strcmp(cmdname, "login") == 0)
	{
		sendLogin(command, cInfo);
	}
	else
		if (strcmp(cmdname, "quit") == 0)
		{
			prepareClientForShuttingDown(cInfo);
		}
		else
		{
			if (cInfo->loggedIn == 0)
			{
				processResult(-1, cInfo);
				return;
			}
			if (strcmp(cmdname, "logout") == 0)
			{
				sendLogout(command, cInfo);
			}
			else
				if (strcmp(cmdname, "getuserlist") == 0)
				{
					sendGetUserList(command, cInfo);
				}
				else
					if (strcmp(cmdname, "getfilelist") == 0)
					{
						sendGetFileList(command, cInfo);
					}
					else
						if (strcmp(cmdname, "upload") == 0)
						{
							sendUpload(command, cInfo);
						}
						else
							if (strcmp(cmdname, "download") == 0)
							{
								sendDownload(command, cInfo); 
							}
							else
								if (strcmp(cmdname, "share") == 0)
								{
									sendShare(command, cInfo);
								}
								else
									if (strcmp(cmdname, "unshare") == 0)
									{
										sendUnShare(command, cInfo);
									}
									else
										if (strcmp(cmdname,"delete") == 0)
										{
											sendDelete(command,cInfo);
										}
										else{
											printf("Unknown command\n");
											fprintf(cInfo->logFile, "Unknown command\n");
											printPrompt(cInfo);
										}
		}
}
void responseServerShutdown(Msg* msg, ClientInfo* cInfo)
{
	cInfo->serverIsShuttingDown = 1;
}
void manageResults(ClientInfo* cInfo)
{
	Msg msg;
	recvMsg(&msg, cInfo->serverSocket);
	if (msg.type == LOGINRESP)
	{
		int result;
		memcpy(&result, msg.payload, sizeof(int));
		responseLogin(msg.payload + 4, result, cInfo);
	}
	else
	if (msg.type == INFO)
	{
		printf("%s", msg.payload);
		fprintf(cInfo->logFile, "%s", msg.payload);
	}
	else
	if (msg.type == GFLRESP)
	{
		responseGetFileList(&msg, cInfo);
	}
	else
	if (msg.type == UPLRESP)
	{
		responseUpload(&msg, cInfo);
	}
	else
	if (msg.type == DLRESP)
	{
		responseDownload(&msg, cInfo);
	}
	else
	if (msg.type == PACKDL)
	{
		handlePackage(&msg, cInfo);
		return;
	}
	else
	if (msg.type == CALLBACK)
	{
		handleCallback(&msg, cInfo);
	}
	else
	if (msg.type == SHRRESP)
	{
		responseShare(&msg, cInfo);
	}
	else
	if (msg.type == USHRRESP)
	{
		responseUnShare(&msg, cInfo);
	}
	else
	if (msg.type == DELETERESP)
	{
		responseDelete(&msg, cInfo);
	}
	else
	if (msg.type == QUITWARNING)
	{
		responseServerShutdown(&msg, cInfo);
		return;
	}
	else
	if (msg.type == SHUTDOWN)
	{
		printf("Server has closed\n");
		fprintf(cInfo->logFile, "Server has closed\n");
		cInfo->running = 0;
		return;
	}
	else
	if (msg.type == NRPACKS)
	{
		initDownload(&msg, cInfo);
		return;
	}
	else
	if (msg.type == PACKACK)
	{
		receivePackACK(&msg, cInfo->uplSessions, cInfo->serverSocket, 0);
		return;
	}
	else
	{
		printf("Unknown response\n");
		fprintf(cInfo->logFile, "Unknown response\n");
		printPrompt(cInfo);
		return;
	}

	printPrompt(cInfo);

}
int main(int argc,char**argv)
{
	if (argc!=3)
		usage(argv[0]);
	
	//int fd;
	struct sockaddr_in to_station;
	ClientInfo clientInfo;


	/*Deschidere socket*/
	int server = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);	
	if (server < 1 )
	{
		printf("Error opening socket\n");
		return 1;
	}

	memset((char*)&to_station, 0, sizeof(to_station));
	
	/*Setare struct sockaddr_in pentru a specifica unde trimit datele*/
	to_station.sin_family = AF_INET;
	to_station.sin_port = htons(atoi(argv[2]));
	inet_aton(argv[1], &to_station.sin_addr);
    
	if (connect(server, (struct sockaddr *) &to_station, sizeof(to_station)) < 0 )
	{
		printf("Error connecting\n");
		return 1;
	}

	fd_set read_fds, tmp_fds;
	FD_ZERO(&read_fds);
	FD_ZERO(&tmp_fds);

	FD_SET(server, &read_fds);
	FD_SET(0, &read_fds);

	FILE* logFile;
	createLogFile(&logFile);
	initClientInfo(&clientInfo, server, logFile);
	char command[MAX_COMMAND_LEN];	
	while (clientInfo.running)
	{
		tmp_fds = read_fds;
		if (select(server+1, &tmp_fds, NULL, NULL, NULL) == -1)
		{
			printf("Error in select\n");
			return 1;
		}

		if (FD_ISSET(0, &tmp_fds))
		{
			//read from keyboard

			fgets(command, MAX_COMMAND_LEN, stdin);
			if (clientInfo.serverIsShuttingDown)
			{
				printf("Server is shutting down...\n");
				fprintf(clientInfo.logFile, "Server is shutting down...\n");
				printPrompt(&clientInfo);
			}
			else
			if (clientInfo.clientIsShuttingDown)
			{
				printf("Client is shutting down...\n");
				fprintf(clientInfo.logFile, "Client is shutting down...\n");
			}
			else
				sendCommand(command, &clientInfo);

		}
		if (FD_ISSET(server, &tmp_fds))
		{

			//receive results from server
			manageResults(&clientInfo);



		}
		if (clientInfo.clientIsShuttingDown && clientInfo.nrUplSessions == 0 && clientInfo.nrDlFiles == 0)
		{
			sendQuit("quit", &clientInfo);
		}



	}

	/*Inchidere socket*/
	close(clientInfo.serverSocket);
	
	return 0;
}
