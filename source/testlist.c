#include "list.h"
#include <stdio.h>
#include <stdlib.h>
void printlist(FileList* l)
{
    while (l)
    {
        printf("%s \n", l->file->name);
        l = l->next;
    }
        
}
int main()
{
   FileList* l = NULL;
   ServerFile* f, *g, *h;
    f = (ServerFile*)malloc(sizeof(ServerFile));
    g = (ServerFile*)malloc(sizeof(ServerFile));
    h = (ServerFile*)malloc(sizeof(ServerFile));
   strcpy(f->name, "file");
   strcpy(g->name, "other");
   strcpy(h->name, "file2");
   addFile(&l, f);
   addFile(&l, g);
   addFile(&l, h);
  removeFile(&l, "file");
  removeFile(&l, "other");
  removeFile(&l, "file2");
   printlist(l);

   return 0;
}
