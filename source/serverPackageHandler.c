#include "../header/serverPackageHandler.h"
#include <unistd.h>
#include <fcntl.h>
void sendPackACK(int client, char* filename, char* owner)
{
	//pack ack contains owner + filename

	Msg msg;
	msg.type = PACKACK;
	strcpy(msg.payload, owner);
	strcpy(msg.payload + strlen(owner) + 1, filename);
	msg.len = strlen(owner) + strlen(filename) + 2;
	sendMsg(&msg, client);
}
void handlePackage(int client, Msg* msg, ServerInfo* serverInfo)
{
	//pack contains owner + filename + data 

	char* owner = msg->payload;
	char* filename = msg->payload + strlen(owner) + 1;
	char* data = msg->payload + strlen(owner) + 1 + strlen(filename) + 1;
	
	char* filePath = getfilepath(filename, owner);
	
	int fd;
	fd = open(filePath, O_WRONLY | O_CREAT | O_APPEND ,  0644);
	write(fd, data, msg->len);
	close(fd);

	TransferFile* ulfile = findTransferFile(serverInfo->uploadingFiles, filename, owner);
	ulfile->nrPacksLeft--;

	if (ulfile->nrPacksLeft == 0)
	{
		moveTransferFileToUser(serverInfo, ulfile, ulfile->owner, 0);
		Msg smsg;
		smsg.type = CALLBACK;
		long int filesz = getFileSize(filePath);
		memcpy(smsg.payload, &filesz, sizeof(long int));
		strcpy(smsg.payload + sizeof(long int), owner);
		strcpy(smsg.payload + sizeof(long int) + strlen(owner) + 1, filename);

		smsg.len = strlen(filename) + strlen(owner ) + 6;
		printf("sending callback message of file %s of %s with bytes %ld\n", filename, owner, filesz);
		sendMsg(&smsg, client);
	}
	else
		sendPackACK(client, filename, owner);

	free(filePath);

}
