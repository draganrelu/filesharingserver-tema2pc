#include "../header/serverFileHandler.h"
#include <dirent.h>
#include <sys/types.h>
#include "../header/clientUserHandler.h"

void createDirectories(ServerInfo* sInfo)
{
	int i;
	struct stat st = {0};
	for (i = 0; i < sInfo->nrUsers; i++)
	{
		char* usrName = sInfo->users[i].name;
		if (stat(usrName, &st) == -1) //directory doesn't exist
			mkdir(usrName, 0700);
	}
}

ServerFile* getUserFile(char* filename, User* usr)
{
	List* flist = usr->files;
	while (flist)
	{
	    ServerFile* curFile = (ServerFile*)flist->info;
	    if (strcmp(curFile->name, filename) == 0)
	    {
		return curFile;
	    }
	    flist = flist->next;
	}
	return NULL;

}

TransferFile* getFileInTransfer(char* filename, ServerInfo* sInfo)
{
	List* upllist = sInfo->uploadingFiles;
	while (upllist)
	{
	    TransferFile* curFile = (TransferFile*)upllist->info;
	    if (strcmp(curFile->name, filename) == 0)
	    {
			return curFile;
	    }
	    upllist = upllist->next;
	}
	List* dllist = sInfo->downloadingFiles;
	while (dllist)
	{
	    TransferFile* curFile = (TransferFile*)dllist->info;
	    if (strcmp(curFile->name, filename) == 0)
	    {
			return curFile;
	    }
	    dllist = dllist->next;
	}
	return NULL;
}

void printTransferFiles(ServerInfo* sInfo)
{
	List* flist = sInfo->uploadingFiles;
	printf("Uploading files: ");
	while (flist)
	{
		TransferFile* curFile = (TransferFile*)flist->info;
		printf("(%s, %s) ", curFile->owner, curFile->name);
		flist = flist->next;
	}
	printf("\nDownloading files: ");
	flist = sInfo->downloadingFiles;
	while (flist)
	{
		TransferFile* curFile = (TransferFile*)flist->info;
		printf("(%s, %s) ", curFile->owner, curFile->name);
		flist = flist->next;
	}
	printf("\n");
}

char* getfilepath(char* filename, char* usrName)
{
	char* path = (char*) calloc((MAX_FILENAME_LEN + 24) , sizeof(char));
	sprintf(path, "%s/%s", usrName, filename);
	return path;
}

void printFilesOfUsers(ServerInfo* sInfo)
{
	int i;
	for (i = 0; i < sInfo->nrUsers; i++)
	{
		List* flist = sInfo->users[i].files;
		printf("User %s has files:\n ", sInfo->users[i].name);
		while (flist)
		{
			printf("%s %d %d\n", ((ServerFile*)flist->info)->name, ((ServerFile*)flist->info)->type, ((ServerFile*)flist->info)->size);
			flist = flist->next;
		}
		printf("\n");
	}
}

void moveTransferFileToUser(ServerInfo* serverInfo, TransferFile* tf, char* usrName, int type)
{
	//type - 1 - downloading
	//type - 0 - uploading

	ServerFile* newFile = (ServerFile*)malloc(sizeof(ServerFile));
	strcpy(newFile->name, tf->name);
	strcpy(newFile->owner, tf->owner);
	newFile->type = tf->type;
	char* filepath = getfilepath(newFile->name, newFile->owner);
	newFile->size = getFileSize(filepath);
	free(filepath);
	
	printf("Moving the file %s of size %d bytes to database\n", newFile->name,newFile->size);
	int usrId = findUser(serverInfo->users, serverInfo->nrUsers, usrName);
	addToList(&serverInfo->users[usrId].files, newFile); 
	
	printTransferFiles(serverInfo);
	if (type == 0)
	{
		removeFromList(&serverInfo->uploadingFiles, tf, eqTransferFile);
		serverInfo->nrUplFiles--;
	}
	else
	{
		removeFromList(&serverInfo->downloadingFiles, tf, eqTransferFile);
		serverInfo->nrDlFiles--;
	}

}
void readSharedFiles(ServerInfo* sInfo)
{
	int nrfiles;
	fscanf(sInfo->fshrd, "%d\n", &nrfiles);
	char line[MAX_LINE_SIZE];

	int i;
	printf("Shared files:\n");
	for (i = 0; i < nrfiles; i++)
	{
		ServerFile* newFile = (ServerFile*)malloc(sizeof(ServerFile));
		fgets(line, MAX_LINE_SIZE, sInfo->fshrd);
		char* usr = strtok(line, ":");
		int usrId;
		if ((usrId = findUser(sInfo->users, sInfo->nrUsers, usr)) == -1)
		{
			free(newFile);
			printf("User %s does not exist\n", usr);
			continue;
		}
		strcpy(newFile->owner, usr); 
		
		char* filename = strtok(NULL, "\n");
		char* filepath = getfilepath(filename, usr);
		if (!fileExists(filepath))
		{
			free(newFile);
			free(filepath);
			printf("File %s does not exist\n", filename);
			continue;
		}

		strcpy(newFile->name, filename);
		newFile->type = 1; //shared
		newFile->size = getFileSize(filepath);
		printf("%s: %s\n", newFile->owner, newFile->name);
		//add file to user
		addToList(&sInfo->users[usrId].files, newFile);
		free(filepath);
	}
	printf("\n");
}
int isFile(const char* path)
{
	struct stat st;
	stat(path, &st);
	return S_ISREG(st.st_mode);
}
void readPrivateFiles(ServerInfo* sInfo)
{
	int i;
	for (i = 0; i < sInfo->nrUsers; i++)
	{
		char* usrName = sInfo->users[i].name;
		//directory exists, it has previously been created
		DIR *usrDir = opendir(usrName);
		struct dirent* ent;
		char* filepath;
		while ((ent = readdir(usrDir)) != NULL)
		{
			filepath = getfilepath(ent->d_name, usrName);
			if (isFile(filepath) && getUserFile(ent->d_name, &sInfo->users[i]) == NULL)
			{

				ServerFile* newfile = createServerFile(ent->d_name, usrName, getFileSize(filepath), 0);
				addToList(&sInfo->users[i].files, newfile);
			}
			free(filepath);
		}
		closedir(usrDir);

	}

}
void readUserFile(ServerInfo* sInfo)
{
	fscanf(sInfo->fusrd, "%d\n", &(sInfo->nrUsers));
	sInfo->users = (User*)malloc(sInfo->nrUsers * sizeof(User));
	int i;

	//add users to system
	printf("Users:\n");
	for (i = 0; i < sInfo->nrUsers; i++)
	{
		fscanf(sInfo->fusrd, "%s %s\n", (sInfo->users)[i].name, (sInfo->users)[i].passwd);
		(sInfo->users)[i].usrID = i;
		(sInfo->users)[i].nrFiles = 0;
		(sInfo->users)[i].files = NULL;
		printf("%s %s\n", (sInfo->users)[i].name, (sInfo->users)[i].passwd);
	}
	printf("\n");
}
void readConfigFiles(ServerInfo* sInfo, char* usrCfgName, char* shrCfgName)
{

	sInfo->fusrd = fopen(usrCfgName, "rt");
	if (!(sInfo->fusrd))
	{
		printf("File %s does not exist\n", usrCfgName);
		return;
	}
	sInfo->fshrd = fopen(shrCfgName, "rt");
	if (!(sInfo->fshrd))
	{
		printf("File %s does not exist.\n", shrCfgName);
		return;
	}

	readUserFile(sInfo);
	createDirectories(sInfo);
	readSharedFiles(sInfo);
	readPrivateFiles(sInfo);

}
