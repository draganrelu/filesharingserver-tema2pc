#include "../header/Files.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

TransferFile* findTransferFile(List* tfiles, char* filename, char* owner)
{
    while (tfiles)
    {
        TransferFile* curFile = ((TransferFile*)tfiles->info);
        if (strcmp(curFile->name, filename) == 0 && strcmp(curFile->owner, owner) == 0)
            return curFile;
        tfiles = tfiles -> next;

    }
    return NULL;
}
TransferFile* createTransferFile(char* name, char* owner, int type, int nrPacksLeft)
{
    TransferFile* res = (TransferFile*)malloc(sizeof(TransferFile));
    strcpy(res->name, name);
    strcpy(res->owner, owner);
    res->type = 0;
    res->nrPacksLeft = nrPacksLeft;
    return res;
}
ServerFile* createServerFile(char* name, char* owner, int size, int type)
{
    ServerFile* res = (ServerFile*)malloc(sizeof(ServerFile));
    strcpy(res->name, name);
    strcpy(res->owner, owner);
    res->size = size;
    res->type = 0;
    return res;
}
int eqTransferFile(const void* tf1, const void* tf2)
{
    TransferFile* convf1 = (TransferFile*)tf1;
    TransferFile* convf2 = (TransferFile*)tf2;
    if (strcmp(convf1->name, convf2->name) == 0 && strcmp(convf1->owner, convf2->owner) == 0)
        return 1;
    else
        return 0;
}
int eqServerFile(const void* tf1, const void* tf2)
{
    ServerFile* convf1 = (ServerFile*)tf1;
    ServerFile* convf2 = (ServerFile*)tf2;
    if (strcmp(convf1->name, convf2->name) == 0 && strcmp(convf1->owner, convf2->owner) == 0)
        return 1;
    else
        return 0;

}
long int getFileSize(char* filename)
{
    FILE* f = (FILE*)fopen(filename, "rt");
    fseek(f, 0, SEEK_END);
    long int result = ftell(f);
    fclose(f);
    return result;
}

int fileExists(char* filename)
{
    FILE* f = (FILE*)fopen(filename, "rt");
    int result;
    if (f)
        result = 1;
    else
        result = 0;
    if (result == 1)
    	fclose(f);
    return result;
}

