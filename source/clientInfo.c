#include "../header/clientInfo.h"

void initClientInfo(ClientInfo* cInfo, int serverSocket, FILE* logfile)
{
    cInfo->serverSocket = serverSocket;
    cInfo->logFile = logfile;
    cInfo->running = 1;
    cInfo->clientIsShuttingDown = 0;
    cInfo->serverIsShuttingDown = 0;
    cInfo->loggedIn = 0;
    cInfo->uplSessions = NULL;
    cInfo->downloadingFiles = NULL;
    cInfo->nrDlFiles = 0;
    cInfo->nrUplSessions = 0;
    cInfo->bruteForce = 0;
}
