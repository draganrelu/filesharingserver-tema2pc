#include "../header/serverExecutor.h"
#include <unistd.h>
#include "../header/transferSession.h"
#include "../header/serverPackageHandler.h"
#include "../header/clientUserHandler.h"

void executeLogin(char* command, int clientSock, ServerInfo* sInfo)
{
	printf("Executing login command from client %d\n", clientSock);
	char* usrName = strtok(NULL, " \n");
	char* passwd = strtok(NULL, "\n");
	int usrId;
	if ((usrId = findUser(sInfo->users, sInfo->nrUsers, usrName)) == -1)
	{
		sendResponse(-3, LOGINRESP, clientSock);
		return ;
	}
	if (strcmp(sInfo->users[usrId].passwd, passwd) != 0)
	{
		sendResponse(-3, LOGINRESP, clientSock);
		return;
	}
	printf("User %s logged in on socket %d\n", sInfo->users[usrId].name, clientSock);
	
	//send back result + the command
	Msg msg;
	int result = 0;
	memcpy(msg.payload, &result, sizeof(int));
	strcpy(msg.payload + 4, command);
	msg.type = LOGINRESP;
	msg.len = strlen(command) + 5;
	sendMsg(&msg, clientSock);

	return;
}
void executeLogout(char* command, int clientSock, ServerInfo* sInfo)
{
	char* usrName = strtok(NULL, " \n");
	printf("User %s from socket %d logged out.\n", usrName, clientSock);
}
void executeDelete(char* command, int clientSock, ServerInfo* sInfo)
{

	char* usrName = strtok(NULL, " \n");
	char* filename = strtok(NULL, "\n");
	int usrId = findUser(sInfo->users, sInfo->nrUsers, usrName);
	if (usrId < 0)
	{
		sendResponse(-11, DELETERESP, clientSock);
		return;
	}
	ServerFile* toRemove = getUserFile(filename, &sInfo->users[usrId]);
	if (toRemove == NULL)
	{
		printf("File is not on server\n");
		sendResponse(-4, DELETERESP, clientSock);
	}
	else
	{
		if (findTransferFile(sInfo->downloadingFiles, filename, usrName))
		{
			//file is currently downloaded by other users
			printf("File is being downloaded by other users\n");
			sendResponse(-12, DELETERESP, clientSock);
		}
		else
		{
			//it can be deleted
			removeFromList(&sInfo->users[usrId].files, toRemove, eqServerFile);
			char* filepath = getfilepath(filename, sInfo->users[usrId].name);
			unlink(filepath);
			sendResponse(0, DELETERESP, clientSock);
			free(filepath);
		}

	}
}
void executeGetUserList(char* command, int clientSock, ServerInfo* sInfo)
{
	Msg msg;
	memset(msg.payload, 0, sizeof(msg.payload));
	int i;
	for (i = 0; i < sInfo->nrUsers; i++)
	{
		strcat(msg.payload, sInfo->users[i].name);
		strcat(msg.payload, "\n");
	}
	msg.type = INFO;
	msg.len = strlen(msg.payload) + 1;
	sendMsg(&msg, clientSock);
}
void executeGetFileList(char* command, int clientSock, ServerInfo* sInfo)
{
	char* usrName = strtok(NULL, " \n");
	int usrId = findUser(sInfo->users, sInfo->nrUsers, usrName);
	if (usrId == -1)
	{
		printf("usr not found\n");
		sendResponse(-11, GFLRESP, clientSock);
		return;
	}

	Msg msg;
	memset(msg.payload, 0, sizeof(msg.payload));

	List* usrFileList = sInfo->users[usrId].files;
	char auxbuf[BUFLEN] = {'\0'};
	while (usrFileList)
	{
		sprintf(auxbuf, "%s\t%d\tbytes\t", ((ServerFile*)usrFileList->info)->name, 
				((ServerFile*)usrFileList->info)->size);
		strcat(msg.payload + 4, auxbuf);
		if (((ServerFile*)usrFileList->info)->type == 1)
			strcat(msg.payload + 4, "\tSHARED\n");
		else
			strcat(msg.payload + 4, "\tPRIVATE\n");
		usrFileList = usrFileList->next;
	}

	msg.len = strlen(msg.payload+4) + 5;
	msg.type = GFLRESP;
	sendMsg(&msg, clientSock);
}

void initUpload(int client, Msg* msg, ServerInfo* serverInfo)
{
	int nrPacks;
	char* owner = msg->payload + 4;
	char* filename = msg->payload + 4 + strlen(owner) + 1;
	memcpy(&nrPacks, msg->payload, sizeof(int));
	printf("Initializing upload of file %s for user %s, waiting for %d packages\n", filename,owner, nrPacks);
	TransferFile* newtfile = createTransferFile(filename, owner, 0, nrPacks);
	addToList(&serverInfo->uploadingFiles, newtfile);
	serverInfo->nrUplFiles++;
	printTransferFiles(serverInfo);
	sendPackACK(client, filename, owner);
}

void executeUpload(char* command, int clientSock, ServerInfo* sInfo)
{

	char* usrName = strtok(NULL, " \n");
	char* filename = strtok(NULL, "\n");
	printf("Executing an upload: %s %s\n", usrName, filename);
	int result = 0;
	int usrId = findUser(sInfo->users, sInfo->nrUsers, usrName);

	//verifica daca e existent deja pe server
	List* flist = sInfo->users[usrId].files;
	while (flist)
	{
		if (strcmp(((ServerFile*)flist->info)->name, filename) == 0)
		{
			result = -9;
			return;
		}
		flist = flist->next;
	}

	//verifica daca e deja in transfer
	if (findTransferFile(sInfo->uploadingFiles, filename, usrName))
	{
		printf("File already uploading\n");
		sendResponse(-10, UPLRESP, clientSock);
		return;
	}

	//if file is not on server, send the filename also
	printf("File is not on server, proceed with upload\n");
	Msg msg; 
	msg.type = UPLRESP; 
	memcpy(msg.payload, &result, sizeof(int));
	msg.len += strlen(filename) + 5;
	strcpy(msg.payload + 4, filename);
	sendMsg(&msg, clientSock);
}


void executeDownload(char* command, int clientSock, ServerInfo* sInfo)
{
	char* fromUsr = strtok(NULL, " \n");
	char* filepath = strtok(NULL, "\n"); //owner/filename
	char owner[24];
	char filename[MAX_FILENAME_LEN];
	char modifiedFilePath[MAX_FILEPATH_LEN]; 
	int pos = 0;

	//get owner
	while (filepath[pos] != '/')
	{
		owner[pos] = filepath[pos];
		pos++;
	}
	owner[pos] = '\0';

	//get filename
	pos++;
	strcpy(filename, filepath + pos);

	printf("download from %s the file %s with owner %s and with filepath %s\n", fromUsr, filename, owner, filepath);	
	int result = 0;
	ServerFile* foundFile = NULL;
	int usrId;

	if (strcmp(owner, "@") == 0)
	{
		strcpy(owner, fromUsr);
		sprintf(modifiedFilePath, "%s/%s", owner, filename);
	}
	else
		strcpy(modifiedFilePath, filepath);

	if ((usrId = findUser(sInfo->users, sInfo->nrUsers, owner)) < 0)
		result = -11;
	else
	{
		//verifica existenta fisierului pe server
		List* flist = sInfo->users[usrId].files;
		while (flist)
		{
			ServerFile* curFile = (ServerFile*)flist->info;
			if (strcmp(curFile->name, filename) == 0)
			{
				foundFile = curFile;
				if (curFile->type == 0 && strcmp(sInfo->users[usrId].name, fromUsr) != 0)
					//fisier gasit privat
					result = -5; 
				break;

			}
			flist = flist->next;
		}
		if (!foundFile)
			result = -4;
	}
	sendResponse(result, DLRESP, clientSock);

	if (result == 0)
	{

		TransferSession* dls = openSession(modifiedFilePath, fromUsr, &sInfo->dlSessions, &sInfo->nrDlSessions);
		printf("Session created with %s %s\n", modifiedFilePath, fromUsr);
		TransferFile* tfile = createTransferFile(filename, owner, 1,dls->nrPacksLeft); 
		addToList(&sInfo->downloadingFiles, tfile);
		sendNrPacks(dls, clientSock);
		printTransferFiles(sInfo);
	}

}

void executeQuit(int clientSock, ServerInfo* sInfo)
{
	printf("execut comanda quit de la clientul %d\n", clientSock);
}
void executeShare(char* command, int clientSock, ServerInfo* sInfo)
{
	char* owner = strtok(NULL, " \n");
	char* filename = strtok(NULL, "\n");
	int usrId = findUser(sInfo->users, sInfo->nrUsers, owner);
	List* flist = sInfo->users[usrId].files;
	int result = 0;
	ServerFile* foundFile = NULL;
	while (flist)
	{
		ServerFile* curFile = (ServerFile*)flist->info;
		if (strcmp(curFile->name, filename) == 0)
		{
			foundFile = curFile;
			if (curFile->type == 1)
				result = -6;
			break;
		}
		flist = flist->next;
	}
	if (!foundFile)
		result = -4;
	if (result == 0)
		foundFile->type = 1;
	sendResponse(result,SHRRESP, clientSock);
}
void executeUnShare(char* command, int clientSock, ServerInfo* sInfo)
{
	char* owner = strtok(NULL, " \n");
	char* filename = strtok(NULL, "\n");
	int usrId = findUser(sInfo->users, sInfo->nrUsers, owner);
	List* flist = sInfo->users[usrId].files;
	int result = 0;
	ServerFile* foundFile = NULL;
	while (flist)
	{
		ServerFile* curFile = (ServerFile*)flist->info;
		if (strcmp(curFile->name, filename) == 0)
		{
			foundFile = curFile;
			if (curFile->type == 0)
				result = -7;
			break;
		}
		flist = flist->next;
	}
	if (!foundFile)
		result = -4;
	if (result == 0)
		foundFile->type = 0;
	sendResponse(result,USHRRESP, clientSock);
}
