#include "../header/mylist.h"

void addToList(List** list, void* info)
{
    if (!(*list))
    {
        *list = (List*)malloc(1*sizeof(List));
        (*list)->next = NULL;
        (*list)->info = info;
        return;
    }
    else
    {
        List* newhead= (List*)malloc(1*sizeof(List));
        newhead->info = info;
        newhead->next = *list;
        *list = newhead;
    }
}
int removeFromList(List** list, void* info, int(*eq)(const void* a, const void* b))
{
  //check first element
  if (eq((*list)->info, info))
  {
    List* aux = *list;
    *list = aux->next;
    free(aux->info);
    free(aux);
    return 1;
  }
  List* it;
  //check other elements
  for (it = *list; it->next; it = it->next)
  {
    if (eq(it->next->info, info))
    {
      List* aux = it->next;
      it->next = aux->next;
      free(aux->info);
      free(aux);
      return 1;
    }
  }
  return 0;
}

List* findInList(List* list, void* info, int (*eq)(const void* a, const void* b))
{
  List* it;
  for (it = list; it; it = it->next)
  {
    if (eq(it->info, info))
    {
      return it;
    }
  }
  return NULL;
}


