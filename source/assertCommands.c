#include "../header/assertCommands.h"
#include "../header/constants.h"
#include <string.h>
int assert1param(const char* command)
{
    char cmdcpy[MAX_COMMAND_LEN];
    strcpy(cmdcpy, command);
    char* cmd = strtok(cmdcpy, " ");
    char* param1 = strtok(NULL, "\n");
    return (cmd && param1);
}
int assert2params(const char* command)
{
    char cmdcpy[MAX_COMMAND_LEN];
    strcpy(cmdcpy, command);
    char* cmd = strtok(cmdcpy, " ");
    char* param1 = strtok(NULL, " ");
    char* param2 = strtok(NULL, "\n");
    return (cmd && param1 && param2);
}
