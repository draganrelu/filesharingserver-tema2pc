/*
*  	Protocoale de comunicatii: 
*  	Laborator 6: UDP
*	client mini-server de backup fisiere
*/

#include "helpers.h"
#define port 10001
void usage(char*file)
{
	fprintf(stderr,"Usage: %s ip_server port_server file\n",file);
	exit(0);
}

/*
*	Utilizare: ./client ip_server port_server nume_fisier_trimis
*/
int main(int argc,char**argv)
{
	if (argc!=4)
		usage(argv[0]);
	
	//int fd;
	struct sockaddr_in from_station;
	char buf[BUFLEN];


	/*Deschidere socket*/
	int sd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);	
	if (sd == -1)
		printf("error open socket\n");
	memset((char*)&from_station, 0, sizeof(from_station));
	/* Deschidere fisier pentru citire */
	//DIE((fd=open(argv[3],O_RDONLY))==-1,"readfile");
	
	/*Setare struct sockaddr_in pentru a specifica unde trimit datele*/
	from_station.sin_family = AF_INET;
	from_station.sin_port = htons(port);
	inet_aton("127.0.0.1", &from_station.sin_addr);
    
	if (connect(sd, (struct sockaddr *) &from_station, sizeof(from_station)) == -1)
	{
		printf("error connect socket\n");
		return 0;
	}
		
	while (1)
	{
		int res = recv(sd, buf, BUFLEN, 0);
		if (res == -1)
		{
			printf("error receive\n");
			return 0;
			
		}
		if (res == 0)
		{
			printf("CLient2: Conexiune terminata\n");
			break;
		}
		printf("Client2: Am primit de la server: %s\n", buf);
		scanf("%s", buf);
		send(sd, buf, BUFLEN,0);
	}
	/*
	*  cat_timp  mai_pot_citi
	*		citeste din fisier
	*		trimite pe socket
	*/	


	/*Inchidere socket*/
	close(sd);
	
	/*Inchidere fisier*/
	//close (fd);
	return 0;
}
