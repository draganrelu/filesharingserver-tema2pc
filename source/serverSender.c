#include "../header/serverSender.h"
#include <fcntl.h>
#include <unistd.h>
#include <sys/socket.h>

void sendResponse(int result, MsgType type, int clientSock)
{

	Msg msg;
	memcpy(msg.payload, &result, sizeof(int));
	msg.len = 4;
	msg.type = type;
	sendMsg(&msg, clientSock);

}
void initDownload(ServerFile* file, ServerInfo* sInfo, int nrPacks)
{
	TransferFile* newtfile = createTransferFile(file->name, file->owner, file->type, nrPacks);
	printf("adding downloading file in transfer %s\n", newtfile->name);
	addToList(&sInfo->downloadingFiles, newtfile);
	printTransferFiles(sInfo);
	sInfo->nrDlFiles++;
}
void sendFile(int client, ServerFile* file, ServerInfo* sInfo)
{
	int maxPackSize = PAYLOAD_LEN - MAX_FILENAME_LEN - 1;
	int nrPacks;
	if (file->size % maxPackSize == 0)
		nrPacks = file->size / maxPackSize;
	else
		nrPacks = file->size / maxPackSize + 1;
	printf("Sending file %s of size %d in %d packs\n", file->name, file->size, nrPacks);	


	initDownload(file, sInfo,nrPacks); 
	//send nr packs + filename
	Msg msg;
	memcpy(msg.payload, &nrPacks, sizeof(int));
	strcpy(msg.payload + 4, file->name);
	msg.len = 5 + strlen(file->name);
	msg.type = NRPACKS;
	sendMsg(&msg, client);

	memset(msg.payload, 0, sizeof(msg.payload));

	//send pack by pack - filename + data
	char* filePath;
	filePath = getfilepath(file->name, file->owner);
	int fd = open(filePath, O_RDONLY);
	if (fd < 0)
	{
		printf("error deschidere fisier %s\n", file->name);
		exit(1);
	}
	free(filePath);
	int citit;
	int nameLen = strlen(file->name);
	strcpy(msg.payload, file->name);
	char* dataPayload = msg.payload + nameLen + 1;
	msg.type = PACKDL;
	while ((citit = read(fd, dataPayload, maxPackSize)) > 0)
	{
		msg.len = citit;
		sendMsg(&msg, client);
	}

	//remove file form transfer
	removeFromList(&sInfo->downloadingFiles, file, eqTransferFile);
	sInfo->nrDlFiles--;

	//send Callback
	msg.type = CALLBACK;
	sprintf(msg.payload, "Download finished: %s - %d bytes\n", file->name, file->size);
	msg.len = strlen(msg.payload) + 1;
	sendMsg(&msg, client);
}
void sendQuitWarningToClients(ServerInfo* sInfo)
{
	int i;
	for (i = 0; i < sInfo->maxClientId; i++)
	{
		if (sInfo->connectedClients[i] == 1)
		{
			Msg msg;
			msg.type = QUITWARNING;
			strcpy(msg.payload, "Server is shutting down...");
			msg.len = strlen(msg.payload) + 1;
			sendMsg(&msg, i);
		}
	}
	
}
void sendShutDownMessageToClients(ServerInfo* sInfo)
{
	int i;
	for (i = 0; i < sInfo->maxClientId; i++)
	{
		if (sInfo->connectedClients[i] == 1)
		{
			Msg msg;
			msg.type = SHUTDOWN;
			msg.len = 0;
			memset(msg.payload, 0, sizeof(msg.payload));
			sendMsg(&msg, i);
		}
	}
}
void sendResult(int result, int clientSock)
{

	char buf[BUFLEN];
	memcpy(buf, &result, sizeof(int));
	send(clientSock, buf, BUFLEN, 0);
}
