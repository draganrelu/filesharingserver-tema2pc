#include "../header/transferSession.h"
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include "../header/mylist.h"
#include "../header/Files.h"
int eqSession(const void* s1, const void* s2)
{
    TransferSession* conv1 = (TransferSession*)s1;
    TransferSession* conv2 = (TransferSession*)s2;
    if (strcmp(conv1->filename, conv2->filename) == 0 && strcmp(conv1->owner, conv2->owner) == 0)
        return 1;
    else
        return 0;
}
TransferSession* openSession(char* fname, char* owner, List** sessions, int* nrSessions)
{
    TransferSession* trs = (TransferSession*)malloc(sizeof(TransferSession));
    strcpy(trs->filename, fname);
    strcpy(trs->owner, owner);
    trs->maxPackSize = PAYLOAD_LEN - MAX_FILENAME_LEN - 1 - 24 - 1;
    trs->fd = open(fname, O_RDONLY);
    trs->offset = 0;
    trs->nrPacksLeft = calculateNrPacks(trs);

    addToList(sessions, trs);
    (*nrSessions)++;
    return trs;
}

void closeSession(TransferSession* trs, List** sessions, int* nrSessions)
{
    if (trs == NULL)
    {
        printf("NULL error session in close session\n");
        return;
    }
    close(trs->fd);
    removeFromList(sessions, trs, eqSession);
    (*nrSessions)--;
}

int calculateNrPacks(TransferSession* trs)
{
    long int filesize = getFileSize(trs->filename);
    int nrPacks;
    if (filesize % trs->maxPackSize == 0)
        nrPacks = filesize / trs->maxPackSize;
    else
        nrPacks = filesize / trs->maxPackSize + 1;
    return nrPacks;
}

void sendNrPacks(TransferSession* trs, int socket)
{
    //send nr packs + owner + filename

    Msg msg;
     
    memcpy(msg.payload, &trs->nrPacksLeft, sizeof(int));
    strcpy(msg.payload + 4, trs->owner); 
    strcpy(msg.payload + 4 + strlen(trs->owner) + 1, trs->filename);
    msg.len = 6 + strlen(trs->filename) + strlen(trs->owner);
    msg.type = NRPACKS;
    sendMsg(&msg, socket);
}

void sendNextPack(TransferSession* trs, int socket, int type)
{
    //type 0 - pack UL
    //type 1 - pack DL
    //add owner + filename + data

    Msg msg;

    //owner
    strcpy(msg.payload, trs->owner);
    int ownerLen = strlen(trs->owner);
    
    //filename
    strcpy(msg.payload + ownerLen + 1, trs->filename);
    int filenameLen = strlen(trs->filename);

    char* dataPayload = msg.payload + ownerLen + 1 + filenameLen + 1;
    if (type == 0)
        msg.type = PACKUL;
    else
        msg.type = PACKDL;

    //data
    int citit;
    citit = read(trs->fd, dataPayload, trs->maxPackSize);
    msg.len = citit;
    sendMsg(&msg, socket);
    //printf("Pack sent\n");
}

TransferSession* findSession(List* sessions, char* filename, char* owner)
{
    while (sessions)
    {
        TransferSession* curSession = (TransferSession*)sessions->info;
        if (strcmp(curSession->filename, filename) == 0 && strcmp(curSession->owner, owner) == 0)
            return curSession;
        sessions = sessions->next;
    }
    return NULL;

}
void receivePackACK(Msg* msg, List* sessions, int socket, int type)
{
    //pack ack contains owner + filename
    char* owner = msg->payload;
    char* filename = msg->payload + strlen(owner) + 1;
    //printf("received pack ack with %s %s\n", owner, filename);
    TransferSession* trs = findSession(sessions, filename, owner);
   // printf("Received packack, sending next pack for session %s %s, packs left %d\n", trs->filename, trs->owner, trs->nrPacksLeft);
    sendNextPack(trs, socket, type);
    trs->nrPacksLeft--;
}
