#include "../header/server.h"
#include "../header/serverExecutor.h"
#include "../header/clientUserHandler.h"
void usage(char*file)
{
	fprintf(stderr,"Usage: %s server_port users_config_file static_shares_config_file\n",file);
	exit(0);
}

void initServer(ServerInfo* sInfo, char* usrCfgName, char* shrCfgName)
{
	sInfo->nrUplFiles = 0;
	sInfo->nrDlFiles = 0;
	sInfo->uploadingFiles = NULL;
	sInfo->downloadingFiles = NULL;
	sInfo->dlSessions = NULL;
	sInfo->nrDlSessions = 0;
	sInfo->running = 1;
	sInfo->shuttingDown = 0;
	sInfo->nrClientsConnected = 0;
	sInfo->maxClientId = MAX_CLIENTS - 1;
	sInfo->connectedClients = (char*)malloc(MAX_CLIENTS * sizeof(char));
	createDirectories(sInfo);
	readConfigFiles(sInfo, usrCfgName, shrCfgName);

}

void processCommand(char* command, int clientSock, ServerInfo* sInfo)
{
	char cpycmd[MAX_COMMAND_LEN];
	strcpy(cpycmd, command);
	char* cmdName = strtok(cpycmd, " \n");
	if (strcmp(cmdName, "login") == 0)
	{
		executeLogin(command, clientSock, sInfo);
		return;
	}
	if (strcmp(cmdName, "logout") == 0)
	{
		executeLogout(command, clientSock, sInfo);
		return;
	}
	if (strcmp(cmdName, "getuserlist") == 0)
	{
		executeGetUserList(command, clientSock, sInfo);
		return;
	}
	if (strcmp(cmdName, "getfilelist") == 0)
	{
		executeGetFileList(command, clientSock, sInfo);
		return;
	}
	if (strcmp(cmdName, "upload") == 0)
	{
		executeUpload(command, clientSock, sInfo);
		return;
	}
	if (strcmp(cmdName, "download") == 0)
	{
		executeDownload(command, clientSock, sInfo);
		return;
	}
	if (strcmp(cmdName, "quit") == 0)
	{
		executeQuit(clientSock, sInfo);
		return;
	}
	if (strcmp(cmdName, "share") == 0)
	{
		executeShare(command, clientSock, sInfo);
		return;
	}
	if (strcmp(cmdName, "unshare") == 0)
	{
		executeUnShare(command, clientSock, sInfo);
		return;
	}
	if (strcmp(cmdName, "delete") == 0)
	{
		executeDelete(command, clientSock, sInfo);
		return;
	}
}

void handleServerCommand(char* command, ServerInfo* sInfo)
{
	if (strcmp(command, "quit\n") == 0)
	{
		printf("Server is shutting down...\n");
		sendQuitWarningToClients(sInfo);
		sInfo->shuttingDown = 1;
	}
	else
		printf("Unknown command\n");
}
void handleCallback(Msg* msg, ServerInfo* serverInfo)
{
	//callback contains filesz + toUser + owner/filename 
	long int filesz;
	memcpy(&filesz, msg->payload, sizeof(long int));
	char* toUser = msg->payload + sizeof(long int);
	char* filepath = msg->payload + sizeof(long int) + strlen(toUser) + 1;
	
	printf("Download finished for file %s of %s - %ld bytes\n", filepath, toUser, filesz);
	closeSession(findSession(serverInfo->dlSessions, filepath, toUser), &serverInfo->dlSessions, &serverInfo->nrDlSessions);
	char filename[MAX_FILENAME_LEN];
	char owner[25];
	int pos = 0;
	while (filepath[pos] != '/')
	{
		owner[pos] = filepath[pos];
		pos++;
	}
	owner[pos] = '\0';
	pos++;
	strcpy(filename, filepath + pos);
	removeFromList(&serverInfo->downloadingFiles, findTransferFile(serverInfo->downloadingFiles, filename, owner), eqTransferFile); 
}

void manageRequest(int client, char* buf, ServerInfo* serverInfo)
{
	Msg msg;
	memcpy(&msg, buf, sizeof(Msg));
	if (msg.type == COMMAND)
	{
		processCommand(msg.payload, client, serverInfo);
		return;
	}
	if (msg.type == PACKUL)
	{
		handlePackage(client, &msg, serverInfo);
		return;	
	}
	if (msg.type == NRPACKS)
	{
		initUpload(client, &msg, serverInfo);
		return;
	}
	if (msg.type == PACKACK)
	{
		receivePackACK(&msg, serverInfo->dlSessions, client, 1);
		return;
	}
	if (msg.type == CALLBACK)
	{
		handleCallback(&msg,serverInfo);
		return;

	}
	printf("Unknown Request\n");
}

int main(int argc,char**argv)
{
	if (argc!=4)
		usage(argv[0]);
	
	char buf[BUFLEN];
	ServerInfo serverInfo;

	fd_set read_fds;
	fd_set tmp_fds;
	int fdmax;

	FD_ZERO(&read_fds);
	FD_ZERO(&tmp_fds);

	/*Deschidere socket*/
	int server = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);	

	if (server < 0)
	{
		printf("Error opening server\n");
		exit(1);
	}
	printf("Server opened at socket %d\n\n", server);
	/*Setare struct sockaddr_in pentru a asculta pe portul respectiv */
	struct sockaddr_in addr;
	memset((char*)&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(atoi(argv[1]));
	inet_aton("127.0.0.1", &(addr.sin_addr));

	/* Legare proprietati de socket */
	if (bind (server, (struct sockaddr*) &addr, sizeof(addr)) < 0)
	{
		printf("error binding\n");
		exit(1);
	}

    if (listen(server, MAX_CLIENTS) < 0)
	{
		printf("error listening\n");
		close(server);
		exit(1);
	}

	initServer(&serverInfo, argv[2], argv[3]);

	printFilesOfUsers(&serverInfo);

	FD_SET(server, &read_fds);
	FD_SET(0, &read_fds);
	fdmax = server;
	int i;
	int client;
	struct sockaddr_in cli_addr;
	socklen_t cliaddr_len;
	while (serverInfo.running) 

	{
		tmp_fds = read_fds;

		if (select( fdmax + 1, &tmp_fds, NULL, NULL, NULL) == -1)
		{
			printf("error in select\n");
			exit(1);
		}
		for (i = 0 ; i <= fdmax; i++)
		{
			if (FD_ISSET(i, &tmp_fds))
			{
				if (i == server) //se conecteaza un nou client
				{
					if ((client = accept(server, (struct sockaddr*)&cli_addr, &cliaddr_len)) == -1)
					{
						printf("error in accept\n");
						close(server);
						exit(1);
					}
					else
					{
						addClient(client, &serverInfo);
						printConnectedClients(&serverInfo);
						FD_SET(client, &read_fds);
						if (client > fdmax)
							fdmax = client;
						printf("New connection from %s, port %d, socket %d\n", inet_ntoa(cli_addr.sin_addr), ntohs(cli_addr.sin_port), client);

					}
				}
				else
				if (i == 0) //primesc date de la tastatura
				{
					char command[MAX_COMMAND_LEN];
					fgets(command, MAX_COMMAND_LEN, stdin);
					handleServerCommand(command, &serverInfo);
				}
				else //primesc date de la un client
				{
					memset(buf, 0, BUFLEN);
					int n;
					if ((n = recv(i, buf, sizeof(buf), 0)) <= 0)
					{
						if (n == 0)
						{
							//conexiunea s-a inchis
							removeClient(i, &serverInfo); 
							printf("socket %d hung up\n", i);
						}
						else
						{
							printf("error in recv\n");
							exit(1);
						}
						close(i);
						FD_CLR(i, &read_fds);
					}
					else
					{
						manageRequest(i, buf, &serverInfo);
					}

					

				}

			}
		}

		if (serverInfo.shuttingDown && serverInfo.nrUplFiles == 0 && serverInfo.nrDlFiles == 0 && serverInfo.nrDlSessions == 0)
		{
			sendShutDownMessageToClients(&serverInfo);
			serverInfo.running = 0;
		}
	}

	printf("Connection finished\n");
	/*Inchidere socket*/	
	close(server);
	
	
	/*Inchidere fisiere*/
//	fclose(serverInfo.fusrd);
//	fclose(serverInfo.fshrd);
	return 0;
}
