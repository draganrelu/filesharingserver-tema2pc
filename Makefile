build:
	gcc -g -Wall source/client.c source/Msg.c source/Files.c source/clientInfo.c source/transferSession.c source/mylist.c source/assertCommands.c -o client
	gcc -g -Wall  source/mylist.c source/Msg.c source/Files.c source/serverFileHandler.c source/serverPackageHandler.c source/transferSession.c source/serverSender.c source/serverExecutor.c source/clientUserHandler.c source/server.c -o server
	
r-client:
	./client 127.0.0.1 10001
r-server:
	./server 10001 users.cfg shares.cfg
clean:
	rm -f client server
	

